<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/check-connect',function () {
	if (DB::connection()->getDatabaseName()) {
		return "YES! successfully connected to the DB: " .DB::
			connection()->getDatabaseName();
	}else{
		return 'connection False !!';
	}
});

Route::get('/index' , 'Usercontroller@index');
Route::get('/product' , 'Usercontroller@product');
Route::get('/about' , 'Usercontroller@about');
Route::get('/contact' , 'Usercontroller@contact');
Route::post('/login' , 'Usercontroller@insert_data2');
Route::get('/login' , 'Usercontroller@login');
Route::get('/register' , 'Usercontroller@register');
Route::post('/index', 'Usercontroller@checkLogin');
Route::post('/about' , 'Usercontroller@about');
Route::post('/register' , 'Usercontroller@register');
Route::get('/shoping-cart' , 'Usercontroller@shopingcart');
Route::get('/blog' , 'Usercontroller@blog');
Route::get('/admin/home' , 'Usercontroller@adminhome');
Route::get('/admin/member' , 'Usercontroller@adminmember');
Route::get('/admin/owner' , 'Usercontroller@adminowner');
Route::get('/admin/self' , 'Usercontroller@adminself');
Route::get('/logout' , 'Usercontroller@logout');
Route::get('/owner/home' , 'Usercontroller@ownerhome');
Route::get('/owner/self' , 'Usercontroller@ownerself');
Route::post('/owner/home' , 'Usercontroller@ownerhome');

Route::get('/owner/addproduct','Usercontroller@owneraddproduct');

Route::get('/owner/editproduct','Usercontroller@ownereditproduct');

Route::post('/insert_product' , 'Usercontroller@insert_product');
Route::get('/owner/update_product' , 'Usercontroller@update_product');
Route::get('/updatemember', 'Usercontroller@updatemember');
Route::get('/owner/reviewmember', 'Usercontroller@reviewmember');
Route::get('/owner/showproduct', 'Usercontroller@showproduct');


Route::post('/review' , 'Usercontroller@insert_review');
Route::get('/owner/delete_product/{id}' , 'Usercontroller@delete_product');
Route::get('/owner/deleteproduct' , 'Usercontroller@ownerdeleteproduct');

Route::get('/owner/edit_product/{id}' , 'Usercontroller@edit_product');
Route::post('/owner/update_product' , 'Usercontroller@update_product2');

Route::post('/update_product3' , 'Usercontroller@update_product3');
Route::post('/owner/update_product3' , 'Usercontroller@update_product3');

Route::get('/owner/edit' , 'Usercontroller@edit');

Route::get('/admin/delete_data/{id}' , 'Usercontroller@delete_data');
Route::get('/admin/edit_data/{id}' , 'Usercontroller@edit_data');
Route::post('/admin/update_data' , 'Usercontroller@update_data');

Route::post('/admin/insert_data' , 'Usercontroller@insert_data');
Route::post('/admin/update_data2' , 'Usercontroller@update_data2');
Route::get('/admin/delete_data2/{id}' , 'Usercontroller@delete_data2');
Route::get('/admin/edit_data2/{id}' , 'Usercontroller@edit_data2');

Route::post('/admin/update_data3' , 'Usercontroller@update_data3');
Route::post('/admin/edit_data3' , 'Usercontroller@edit_data3');

Route::get('/owner/delete_data3/{id}' , 'Usercontroller@delete_data3');
Route::get('/owner/edit_data3/{id}' , 'Usercontroller@edit_data3');
Route::post('/owner/update_data3' , 'Usercontroller@update_data3');

Route::post('/owner/update_data4' , 'Usercontroller@update_data4');
Route::post('/update_data5' , 'Usercontroller@update_data5');






