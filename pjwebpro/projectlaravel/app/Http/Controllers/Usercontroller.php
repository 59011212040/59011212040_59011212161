<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Usercontroller extends Controller
{

	public function index(){
		$data = DB::table('product')->get();
		return view('index',['data' => $data->all()]);
	}
	public function product(){
		$data = DB::table('product')->get();
		return view('product',['data' => $data->all()]);
	}
	public  function about(){
		return view('about');
	}
	public  function contact(){
		return view('contact');
	}
	public  function login(){
		return view('login');
	}
	public  function register(){
		return view('register');
	}
	public  function shopingcart(){
		return view('shoping-cart');
	}
	public  function blog(){
		$review = DB::table('review')->get();
		return view('blog',['reviews' => $review]);
	}
	public  function updatemember(){
		$id = session('id');
		$member = DB::table('member')->where('id',$id)->get();
		return view('updatemember',['members' => $member]);
	}
	//Admin
	public  function adminhome(){
		return view('admin/home');
	}
	public  function adminmember(){
		$member = DB::table('member')->get();
		return view('admin/member',['members' => $member]);
	}

	public  function adminowner(){
		$owner = DB::table('owner')->get();
		return view('admin/owner',['owners' => $owner]);

	}
	public  function adminself(){
		$id = session('id');
		$admin = DB::table('admin')->where('id',$id)->get();
		return view('admin/self',['admins' => $admin]);
	}
	//เจ้าของร้าน
	public  function ownerhome(){
		return view('owner/home');
	}
	public  function ownerself(){
		$id = session('id');
		$owner = DB::table('owner')->where('id',$id)->get();
		return view('owner/self',['owners' => $owner]);
	}
	public function owneraddproduct(){
		return view('owner/addproduct');
	}
	public function ownerdeleteproduct(){

		$product = DB::table('product')->get();
		return view('owner/deleteproduct',['products' => $product]);
	}

	public function ownereditproduct(){
		$product = DB::table('product')->get();
		return view('owner/editproduct',['products' => $product]);
	}
	public function showproduct(){
		$product = DB::table('product')->get();
		return view('owner/showproduct',['products' => $product]);
	}
	public  function reviewmember(){
		$review = DB::table('review')->get();
		return view('owner/reviewmember',['reviews' => $review]);
	}
	//logout
	public  function logout(){
		session_start() ;
		session()->flush();
		return redirect('login');
	}
	//login
	public function checkLogin(Request $result1){
		session_start();
		$data = $result1->all();
		$user = $data['username'];
		$pass = $data['password'];
		$array = array(
			'Username' => $user,
			'Password' => $pass
		);
		$ans = DB::table('member')->where($array)->get();
		$row_count = count($ans);

		$ans2 = DB::table('admin')->where($array)->get();
		$row_count2 = count($ans2);

		$ans3 = DB::table('owner')->where($array)->get();
		$row_count3 = count($ans3);

		if($row_count == 1){
			session([
				'username' => $ans[0]->Username,
				'id' => $ans[0]->id,
			]);
			return redirect('index');
		} 
		elseif ($row_count2 == 1) {
			session([
				'username' => $ans2[0]->Username,
				'id' => $ans2[0]->id,
			]);
			return  redirect('admin/home');
		}
		elseif ($row_count3 == 1) {
			session([
				'username' => $ans3[0]->Username,
				'id' => $ans3[0]->id,
			]);
			return  redirect('owner/home');
		}else{
			echo "fail";
		}
	}
	//insert
	//register
	public function insert_data2(Request $request){
		$data = $request->all();
		$result = array(
			'Username' => $data['Username'],
			'mFirstname' => $data['fname'],
			'mLastname' => $data['lname'],
			'mPhone' => $data['phone'],
			'mIdcard' => $data['idcard'],
			'mEmail' => $data['email'],
			'mAddress' => $data['address'],
			'Password' => $data['password'],
		);
		DB::table('member')->insert($result);
		return view('login');
	}

	//admininsertowner
	public function insert_data(Request $request){
		$data = $request->all();
		$result = array(
			'id' => $data['id'],
			'Username' => $data['username'],
			'owner_phone' => $data['phone'],
			'owner_Email' => $data['email'],
			'Password' => $data['password'],
		);
		DB::table('owner')->insert($result);
		return view('/admin/home');
	}
	//addadmin
	public function insert_data3(Request $request){
		$data = $request->all();
		$result = array(
			'id' => $data['id'],
			'Username' => $data['username'],
			'Password' => $data['password'],
		);
		DB::table('admin')->insert($result);
		return view('/admin/home');
	}

	public function insert_product(Request $request){
		$data = $request->all();
		$result = array(
			'pName' => $data['product_name'],
			'pType' => $data['product_type'],
			'pAmount' => $data['product_amount'],
			'pPrice' => $data['product_price'],
			'owner_id' => $data['owner_id'],
			'img_url' => $data['img_url'],
		);

		DB::table('product')->insert($result);
		return redirect('/owner/showproduct');
	}

	public function insert_review(Request $request){
		$data = $request->all();
		$result = array(
			'review_text' => $data['review'],
			'score' => $data['score'],
		);
		DB::table('review')->insert($result);
		return redirect('/blog');
	}
	//delete
	//addmindeleteadmin

	public function delete_data($id){
		DB::table('member')->where('id',$id)->delete();
		return redirect('admin/member');
	}
	//addmindeleteowner
	public function delete_data2($id){
		DB::table('owner')->where('id',$id)->delete();
		return redirect('admin/owner');
	}
	//addmindeleteadmin
	public function delete_data3($id){
		DB::table('admin')->where('id',$id)->delete();
		return redirect('admin/self');
	}

	public function delete_product($id){
		DB::table('product')->where('id',$id)->delete();
		return redirect('owner/deleteproduct');
	}

	//update
	//member
	public function update_data(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Username' => $data['Username'],
			'mFirstname' => $data['fname'],
			'mLastname' => $data['lname'],
			'mPhone' => $data['phone'],
			'mIdcard' => $data['idcard'],
			'mEmail' => $data['email'],
			'mAddress' => $data['address'],
		);
		DB::table('member')->where('id',$id)->update($result);
		return redirect('admin/member');
	}

	//owner
	public function update_data2(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'id' => $data['id'],
			'Username' => $data['username'],
			'owner_phone' => $data['phone'],
			'owner_Email' => $data['email'],
		);
		DB::table('owner')->where('id',$id)->update($result);
		return redirect('admin/owner');
	}

	public function update_product(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'pName' => $data['product_name'],
			'pType' => $data['product_type'],
			'pAmount' => $data['product_amount'],
			'pPrice' => $data['product_price'],
			'owner_id' => $data['owner_id'],
			'img_url' => $data['img_url'],
		);
		DB::table('product')->where('id',$id)->update($result);
		return redirect('owner/showproduct');
	}

	public function update_product2(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'pName' => $data['product_name'],
			'pType' => $data['product_type'],
			'pAmount' => $data['product_amount'],
			'pPrice' => $data['product_price'],
			'owner_id' => $data['owner_id'],
			'img_url' => $data['img_url'],
		);
		DB::table('product')->where('id',$id)->update($result);
		return redirect('owner/edit');
	}
	public function update_product3(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'pName' => $data['product_name'],
			'pType' => $data['product_type'],
			'pAmount' => $data['product_amount'],
			'pPrice' => $data['product_price'],
			'owner_id' => $data['owner_id'],
			'img_url' => $data['img_url'],
		);
		DB::table('product')->where('id',$id)->update($result);
		return redirect('owner/editproduct');
	}

	//admin
	public function update_data3(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Username' => $data['username'],
			'Password' => $data['password'],
		);
		DB::table('admin')->where('id',$id)->update($result);
		return redirect('admin/self');
	}

	//owner
	public function update_data4(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'id' => $data['id'],
			'Username' => $data['username'],
			'owner_phone' => $data['phone'],
			'owner_Email' => $data['email'],
			'Password' => $data['password'],
		);
		DB::table('owner')->where('id',$id)->update($result);
		return redirect('owner/self');
	}

	public function update_data5(Request $request){
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Username' => $data['Username'],
			'mFirstname' => $data['fname'],
			'mLastname' => $data['lname'],
			'mPhone' => $data['phone'],
			'mIdcard' => $data['idcard'],
			'mEmail' => $data['email'],
			'mAddress' => $data['address'],
			'Password' => $data['password'],
		);
		DB::table('member')->where('id',$id)->update($result);
		return redirect('updatemember');
	}

	//edit
	public function edit_data($id){
		$member = DB::table('member')->where('id',$id)->get();
		return view('admin/editmember',['members' => $member]);
	}

	public function edit_data2($id){
		$owner = DB::table('owner')->where('id',$id)->get();
		return view('admin/editowner',['owners' => $owner]);
	}

	public function edit_data3($id){
		$admin = DB::table('admin')->where('id',$id)->get();
		return view('admin/editadmin',['admins' => $admin]);
	}
	public function edit_product($id){
		$product = DB::table('product')->where('id',$id)->get();
		return view('owner/edit',['products' => $product]);
	}

}