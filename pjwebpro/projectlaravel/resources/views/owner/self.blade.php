<!DOCTYPE html>
<html lang="en">
<head>
  <title>About</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.png')}}"/>
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
  <!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/css/animsition.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
  <!--===============================================================================================-->
</head>
<body class="animsition">

  <!-- Header -->
  <header class="header-v4">
    <!-- Header desktop -->
    <div class="container-menu-desktop">
      <!-- Topbar -->
      <div class="top-bar">
        <div class="content-topbar flex-sb-m h-full container">
          <div class="wrap-icon-header flex-w flex-r-m">
            <h5 class="text-success">{{ session('username')}}</h5>
          </div>
        </div>
      </div>

      <div class="wrap-menu-desktop how-shadow1">
        <nav class="limiter-menu-desktop container">

          <!-- Logo desktop -->   

          <!-- Menu desktop -->
          <div class="menu-desktop">
            <ul class="main-menu">
              <li>
                <a href="/index">หน้าหลัก</a>
              </li>
              <li>
                <a href="/owner/self">ข้อมูลส่วนตัว</a>
              </li>
              <li>
                <a href="/owner/reviewmember">รีวิวลูกค้า</a>
              </li>
              <div class="menu-desktop">
                <ul class="main-menu">
                  <li class="active-menu">
                    <a href="/owner/addproduct">สินค้า</a>
                    <ul class="sub-menu">
                      <li><a href="/owner/addproduct">เพิ่มสินค้า</a></li>
                      <li><a href="/owner/deleteproduct">ลบสินค้า</a></li>
                      <li><a href="/owner/editproduct">แก้ไขสินค้า</a></li>
                      <li><a href="/owner/showproduct">คลังสินค้า</a></li>
                    </ul>                 
                  </li>
                </div>
              </ul>
            </div>  

            <!-- Icon header -->
            <div class="wrap-icon-header flex-w flex-r-m">
              <li>
                <a href="/logout">Logout</a>
              </li>
            </div>
          </nav>
        </div>  
      </div>

      <div class="wrap-header-mobile">
        <!-- Logo moblie -->    


        <!-- Icon header -->


        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </div>
      </div>


      <!-- Menu Mobile -->
      <div class="menu-mobile">
        <ul class="topbar-mobile">
          <li>
            <div class="left-top-bar">
            </div>
          </li>
        </ul>

        <ul class="main-menu-m">
          <li>
            <a href="/index">หน้าหลัก</a>
          </li>
          <li>
            <a href="/owner/self">ข้อมูลส่วนตัว</a>
          </li>
          <li>
            <a href="/owner/reviewmember">รีวิวลูกค้า</a>
          </li>
          <div class="menu-desktop">
            <ul class="main-menu">
              <li class="active-menu">
                <a href="/owner/addproduct">สินค้า</a>
                <ul class="sub-menu">
                  <li><a href="/owner/addproduct">เพิ่มสินค้า</a></li>
                  <li><a href="/owner/deleteproduct">ลบสินค้า</a></li>
                  <li><a href="/owner/editproduct">แก้ไขสินค้า</a></li>
                  <li><a href="/owner/showproduct">คลังสินค้า</a></li>
                </ul>                 
              </li>
            </div>
          </ul>
        </div>

        <div class="container">
          <div class="row main">
            <div class="col-lg-3"></div>
            <div class="main-login main-center col-lg-6">
              <h2 class="text-center">ข้อมูลเจ้าของร้าน</h2>
              <form action="/owner/update_data4" method="POST" class="pt-5 pb-5">
                <input type="hidden" name="id" value="<?=$owners[0]->id?>">
                {{ csrf_field()}}
                <div class="form-group">
                  <label for="username" class="cols-sm-2 control-label">Username</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                      <input type="text" class="form-control"  id="username" name="username" placeholder="Enter your Username" value="<?=$owners[0]->Username?>" >
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="name" class="cols-sm-2 control-label">Phone</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                      <input type="text" class="form-control" name="phone" id="phone"  placeholder="Enter your Phone" value="<?=$owners[0]->owner_phone?>" >
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="email" class="cols-sm-2 control-label">E-mail</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                      <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" value="<?=$owners[0]->owner_email?>" >
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="cols-sm-2 control-label">Password</label>
                  <div class="cols-sm-10">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                      <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" value="<?=$owners[0]->Password?>" >
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>




        <!--===============================================================================================--> 
        <script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendor/animsition/js/animsition.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
        <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
        <script>
          $(".js-select2").each(function(){
            $(this).select2({
              minimumResultsForSearch: 20,
              dropdownParent: $(this).next('.dropDownSelect2')
            });
          })
        </script>
        <!--===============================================================================================-->
        <script src="{{asset('vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
        <!--===============================================================================================-->
        <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
        <script>
          $('.js-pscroll').each(function(){
            $(this).css('position','relative');
            $(this).css('overflow','hidden');
            var ps = new PerfectScrollbar(this, {
              wheelSpeed: 1,
              scrollingThreshold: 1000,
              wheelPropagation: false,
            });

            $(window).on('resize', function(){
              ps.update();
            })
          });
        </script>
        <!--===============================================================================================-->
        <script src="{{asset('js/main.js')}}"></script>

      </body>
      </html>