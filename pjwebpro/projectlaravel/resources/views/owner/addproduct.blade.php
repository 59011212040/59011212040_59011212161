<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->  
	<link rel="icon" type="image/png" href="{{asset('images/icons/favicon.png')}}"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
	<!--===============================================================================================-->  
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
	<!--===============================================================================================-->
</head>
<body class="animsition">

	<!-- Header -->
	<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="wrap-icon-header flex-w flex-r-m">
						<h5 class="text-success">{{ session('username')}}</h5>
					</div>
				</div>
			</div>

			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">
					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li>
								<a href="/index">หน้าหลัก</a>
							</li>
							<li>
								<a href="/owner/self">ข้อมูลส่วนตัว</a>
							</li>
							<li>
								<a href="/owner/reviewmember">รีวิวลูกค้า</a>
							</li>
							<div class="menu-desktop">
								<ul class="main-menu">
									<li class="active-menu">
										<a href="/owner/addproduct">สินค้า</a>
										<ul class="sub-menu">
											<li><a href="/owner/addproduct">เพิ่มสินค้า</a></li>
											<li><a href="/owner/deleteproduct">ลบสินค้า</a></li>
											<li><a href="/owner/editproduct">แก้ไขสินค้า</a></li>
											<li><a href="/owner/showproduct">คลังสินค้า</a></li>
										</ul>									
									</li>
								</div>
							</ul>
						</div>  
						<!-- Icon header -->
						<div class="wrap-icon-header flex-w flex-r-m">
							<li>
								<a href="/logout">Logout</a>
							</li>
						</div>
					</nav>
				</div>  
			</div>

			<!-- Header Mobile -->
			<div class="wrap-header-mobile">
				<!-- Logo moblie -->    
				<!-- Button show menu -->
				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
			
			<div class="container mt-3 mb-3">
				<div class="row">
					<div class="col-lg-4">
						<form action="/insert_product" method="POST" class="pt-3 pb-3">
							{{ csrf_field() }}
							<h1 class="mt-5 mb-5">Add Product</h1>
							<div class="form-group">
								<label for="Name">Name</label>
								<input type="text" class="form-control" id="product_name" placeholder="Enter Name" name="product_name">
							</div>
							<div class="form-group">
								<label for="Type">Type</label>
								<input type="text" class="form-control" id="product_type" placeholder="Enter Type" name="product_type">
							</div>
							<div class="form-group">
								<label for="Amount">Amount</label>
								<input type="text" class="form-control" id="product_amount" placeholder="Enter Amount" name="product_amount">
							</div>
							<div class="form-group">
								<label for="Price">Price</label>
								<input type="text" class="form-control" id="product_price" placeholder="Enter Prict" name="product_price">
							</div>
							<div class="form-group">
								<label for="Price">Owner</label>
								<input type="text" class="form-control" id="owner_id" placeholder="Enter owner ID" name="owner_id">
							</div>
							<div class="form-group">
								<label for="img_url">Img URL</label>
								<input type="text" class="form-control" id="img_url" placeholder="Enter IMG URL" name="img_url">
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>

				</div>

			</div>



			<!-- Menu Mobile -->
			<div class="menu-mobile">
				<ul class="topbar-mobile">
					<li>
						<div class="left-top-bar">
						</div>
					</li>
				</ul>

				<ul class="main-menu-m">
					<li>
						<a href="/index">หน้าหลัก</a>
					</li>
					<li>
						<a href="/owner/self">ข้อมูลส่วนตัว</a>
					</li>
					<li>
						<a href="/owner/reviewmember">รีวิวลูกค้า</a>
					</li>
					<li>
						<a href="/owner/addproduct">สินค้า</a>
					</li>
					<div class="menu-desktop">
						<ul class="main-menu">
							<li class="active-menu">
								<a href="/owner/addproduct">สินค้า</a>
								<ul class="sub-menu">
									<li><a href="/owner/addproduct">เพิ่มสินค้า</a></li>
									<li><a href="/owner/deleteproduct">ลบสินค้า</a></li>
									<li><a href="/owner/editproduct">แก้ไขสินค้า</a></li>
									<li><a href="/owner/showproduct">คลังสินค้า</a></li>
								</ul>									
							</li>
						</div>
					</ul>
				</div>

				<!-- Modal Search -->
				<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
					<div class="container-search-header">
						<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
							<img src="{{asset('images/icons/icon-close2.png')}}" alt="CLOSE">
						</button>

						<form class="wrap-search-header flex-w p-l-15">
							<button class="flex-c-m trans-04">
								<i class="zmdi zmdi-search"></i>
							</button>
							<input class="plh3" type="text" name="search" placeholder="Search...">
						</form>
					</div>
				</div>
			</header>

			

			<!-- Back to top -->
			<div class="btn-back-to-top" id="myBtn">
				<span class="symbol-btn-back-to-top">
					<i class="zmdi zmdi-chevron-up"></i>
				</span>
			</div>

			<!--===============================================================================================--> 
			<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
			<!--===============================================================================================-->
			<script src="{{asset('vendor/animsition/js/animsition.min.js')}}"></script>
			<!--===============================================================================================-->
			<script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
			<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
			<!--===============================================================================================-->
			<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
			<script>
				$(".js-select2").each(function(){
					$(this).select2({
						minimumResultsForSearch: 20,
						dropdownParent: $(this).next('.dropDownSelect2')
					});
				})
			</script>
			<!--===============================================================================================-->
			<script src="{{asset('vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
			<!--===============================================================================================-->
			<script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
			<script>
				$('.js-pscroll').each(function(){
					$(this).css('position','relative');
					$(this).css('overflow','hidden');
					var ps = new PerfectScrollbar(this, {
						wheelSpeed: 1,
						scrollingThreshold: 1000,
						wheelPropagation: false,
					});

					$(window).on('resize', function(){
						ps.update();
					})
				});
			</script>
			<!--===============================================================================================-->
			<script src="{{asset('js/main.js')}}"></script>

		</body>
		</html>