<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>

</head>
<body>
	
	<div class="container mt-5 mb-5">
		<h1 class="display-4 mt-2 mb-2">Edit Product</h1>
		<div class="row">
			<div class="col-lg-4">
				<form action="/owner/update_product3" method="POST" class="pt-5 pb-5">
					<input type="hidden" name="id" value="<?=$products[0]->id?>">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="product_name">Product Name</label>
						<input type="text" class="form-control" id="product_name" placeholder="Enter Product Name" name="product_name" value="<?=$products[0]->pName?>">
					</div>
					<div class="form-group">
						<label for="product_type">Product Type</label>
						<input type="text" class="form-control" id="product_type" placeholder="Enter Product Type" name="product_type" value="<?=$products[0]->pType?>">
					</div>
					<div class="form-group">
						<label for="product_amount">Product Amount</label>
						<input type="product_amount" class="form-control" id="product_amount" placeholder="Enter Product Amount" name="product_amount" value="<?=$products[0]->pAmount?>">
					</div>
					<div class="form-group">
						<label for="product_price">Product Price</label>
						<input type="text" class="form-control" id="product_prict" placeholder="Enter Product Price" name="product_price" value="<?=$products[0]->pPrice?>">
					</div>
					<div class="form-group">
						<label for="owner_id">Owner ID</label>
						<input type="text" class="form-control" id="owner_id" placeholder="Enter Owner ID" name="owner_id" 
						value="<?=$products[0]->owner_id?>">
					</div>  
					<div class="form-group">
						<label for="img">Img URL</label>
						<input type="img" class="form-control" id="img_url" placeholder="Enter Owner ID" name="img_url" 
						value="<?=$products[0]->img_url?>">
					</div>  
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>

		</div>

	</div>
	<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>
</body>
</html>