<!DOCTYPE html>
<html lang="en">
<head>
	<title>About</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->  
	<link rel="icon" type="image/png" href="{{asset('images/icons/favicon.png')}}"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
	<!--===============================================================================================-->  
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
	<!--===============================================================================================-->
</head>
<body class="animsition">

	<!-- Header -->
	<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
					<div class="wrap-icon-header flex-w flex-r-m">
						<h5 class="text-success">{{ session('username')}}</h5>

					</div>
				</div>
			</div>

			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">

					<!-- Logo desktop -->   

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li>
								<a href="/index">หน้าหลัก</a>
							</li>
							<li>
								<a href="/admin/member">ข้อมูลลูกค้า</a>
							</li>
							<li>
								<a href="/admin/owner">ข้อมูลเจ้าของร้าน</a>
							</li>
							<li>
								<a href="/admin/self">ข้อมูลส่วนตัว</a>
							</li>

						</ul>
					</div>  

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">
						<li>
							<a href="/logout">Logout</a>
						</li>
					</div>
				</nav>
			</div>  
		</div>

		<div class="wrap-header-mobile">
			<!-- Logo moblie -->    
			<!-- Icon header -->
			<!-- Button show menu -->
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>
		<!-- Menu Mobile -->
		<div class="menu-mobile">
			<ul class="topbar-mobile">
				<li>
					<div class="left-top-bar">
					</div>
				</li>
			</ul>

			<ul class="main-menu-m">
				<li>
					<a href="/index">หน้าหลัก</a>
				</li>
				<li>
					<a href="/admin/member">ข้อมูลลูกค้า</a>
				</li>
				<li>
					<a href="/admin/owner">ข้อมูลเจ้าของร้าน</a>
				</li>
				<li>
					<a href="/admin/self">ข้อมูลส่วนตัว</a>
				</li>

			</ul>
		</div>

		<div class="container mb-5">
			<h1 class="mt-5 mb-5">เจ้าของร้าน</h1>
			<div class="row">
				<div class="form-row w-100">
					<div class="table-responsive">
						<table class="table table-hover"> 
							<thead>
								<th>#</th>
								<th>Id</th>
								<th>Username</th>
								<th>Phone</th>
								<th>E-mail</th>
								<th></th>
								<th></th>
							</thead>
							<tbody>
								<?php
								if (count($owners) > 0 ) {
									$i = 1;
									foreach ($owners as $owner) {
										echo "<tr>";
										echo "<td>".$i."</td>";
										echo "<td>".$owner->id."</td>";
										echo "<td>".$owner->Username."</td>";
										echo "<td>".$owner->owner_phone."</td>";
										echo "<td>".$owner->owner_email."</td>";
										echo "<td><a href='delete_data2/".$owner->id."' title=''>Delete</a></td>";
										echo "<td><a href='edit_data2/".$owner->id."' 
										title=''>Edit</a></td>";
										echo "</tr>";
										$i++;
									}
								}
								else {
									echo "<tr><td align='center' colspan='4'>Not Found Data</td></tr>";
								}
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<br><br>
		<div class="container mt-5 mb-5">

			<div class="row">
				<form action="/admin/insert_data" method="POST" class="pt-5 pb-5">
					{{ csrf_field()}}
					<form>
						<h3 class="mt-5 mb-5">เพิ่มเจ้าของร้าน</h3>
						<div class="form-group">
							<label for="username">Id</label>
							<input type="text" class="form-control" id="id" name="id" aria-describedby="id" placeholder="Enter Id">
						</div>
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" class="form-control" id="username" name="username" aria-describedby="username" placeholder="Enter Username">
						</div>
						<div class="form-group">
							<label for="fname">Phone</label>
							<input type="text" class="form-control" id="phone" name ="phone" placeholder="Enter First Phone">
						</div>
						<div class="form-group">
							<label for="lname">E-mail</label>
							<input type="email" class="form-control" id="email" name ="email" placeholder="Enter Last E-mail">
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name ="password"placeholder="Enter Password">
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</form>
			</div>
		</div> 


		<div class="btn-back-to-top" id="myBtn">
			<span class="symbol-btn-back-to-top">
				<i class="zmdi zmdi-chevron-up"></i>
			</span>
		</div>

		<!--===============================================================================================--> 
		<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('vendor/animsition/js/animsition.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
		<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
		<script>
			$(".js-select2").each(function(){
				$(this).select2({
					minimumResultsForSearch: 20,
					dropdownParent: $(this).next('.dropDownSelect2')
				});
			})
		</script>
		<!--===============================================================================================-->
		<script src="{{asset('vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
		<!--===============================================================================================-->
		<script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
		<script>
			$('.js-pscroll').each(function(){
				$(this).css('position','relative');
				$(this).css('overflow','hidden');
				var ps = new PerfectScrollbar(this, {
					wheelSpeed: 1,
					scrollingThreshold: 1000,
					wheelPropagation: false,
				});

				$(window).on('resize', function(){
					ps.update();
				})
			});
		</script>
		<!--===============================================================================================-->
		<script src="{{asset('js/main.js')}}"></script>

	</body>
	</html>