<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>

	<link  rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">

	<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>

</head>
<body>
	
	<div class="container">
		<div class="row main">
			<div class="col-lg-3">
				
			</div>
			<div class="main-login main-center col-lg-6">
				<h2 class="text-center">แก้ไขข้อมูลส่วนตัว</h2>
				<form action="/admin/update_data3" method="POST" class="pt-5 pb-5">
					<input type="hidden" name="id" value="<?=$admins[0]->id?>">
					{{ csrf_field()}}
					<div class="form-group">
						<label for="username" class="cols-sm-2 control-label">Username</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
								<input type="text" class="form-control"  id="username" name="username" placeholder="Enter your Username" value="<?=$admins[0]->Username?>" >
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
								<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" value="<?=$admins[0]->Password?>" >
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>