<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>

	<link  rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">

	<script type="text/javascript" src="{{ asset('js/jquery.js')}}"></script>

</head>
<body>
	
	<div class="container">
		<div class="row main">
			<div class="col-lg-3">
			</div>
			<div class="main-login main-center col-lg-6">
				<h2 class="text-center">แก้ไขข้อมูลสมาชิก</h2>
				<form action="/admin/update_data" method="POST" class="pt-5 pb-5">
					<input type="hidden" name="id" value="<?=$members[0]->id?>">
					{{ csrf_field()}}
					<div class="form-group">
						<label for="username" class="cols-sm-2 control-label">Username</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="Username" id="Username"  placeholder="Enter your Username" value="<?=$members[0]->Username?>" >
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">First Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="fname" id="fname"  placeholder="Enter your First Name" value="<?=$members[0]->mFirstname?>" >
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">Last Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="lname" id="lname"  placeholder="Enter your Last Name" value="<?=$members[0]->mLastname?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">Phone</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="phone" id="phone"  placeholder="Enter your Phone" value="<?=$members[0]->mPhone?>" >
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">ID Card</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="idcard" id="idcard"  placeholder="Enter your ID Card" value="<?=$members[0]->mIdcard?>">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Your Email</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
								<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" value="<?=$members[0]->mEmail?>" >
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="address" class="cols-sm-2 control-label">Address</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="address" id="address"  placeholder="Enter your Address" value="<?=$members[0]->mAddress?>">
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>