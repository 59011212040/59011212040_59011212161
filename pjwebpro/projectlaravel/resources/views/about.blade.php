<!DOCTYPE html>
<html lang="en">
<head>
	<title>About</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/linearicons-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
</head>
<body class="animsition">
	
	<!-- Header -->
	<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->
			<div class="top-bar">
				<div class="content-topbar flex-sb-m h-full container">
				</div>
			</div>

			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">
					
					<!-- Logo desktop -->		
					<h2 class="text-monospace">AMMNOY SHOP</h2>
					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<li>
								<a href="/index">Home</a>
							</li>

							<li>
								<a href="/product">Shop</a>
							</li>
							<li>
								<a href="/blog">Review</a>
							</li>

							<li class="active-menu">
								<a href="/about">About</a>
							</li>

							<li>
								<a href="/contact">Contact</a>
							</li>
						</ul>
					</div>	

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">
						<div class="menu-desktop">
							<ul class="main-menu">
								<li class="active-menu">
									<a href="index">{{ session('username')}}</a>
									<ul class="sub-menu">
										<li><a href="/updatemember">ข้อมูลส่วนตัว</a></li>
										<li><a href="/logout">ออกจากระบบ</a></li>
									</ul>
								</li>
							</div>
						</nav>
					</div>	
				</div>

				<!-- Header Mobile -->
				<div class="wrap-header-mobile">
					<!-- Logo moblie -->		
					<h2 class="text-monospace">AMMNOY SHOP</h2>

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m m-r-15">
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
							<i class="zmdi zmdi-search"></i>
						</div>

						<div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="2">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>

						<a href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti" data-notify="0">
							<i class="zmdi zmdi-favorite-outline"></i>
						</a>
					</div>

					<!-- Button show menu -->
					<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</div>
				</div>


				<!-- Menu Mobile -->
				<div class="menu-mobile">
					<ul class="topbar-mobile">
						<li>
							<div class="left-top-bar">
								Free shipping for standard order over $100
							</div>
						</li>

						<li>
							<div class="right-top-bar flex-w h-full">
								<a href="#" class="flex-c-m p-lr-10 trans-04">
									Help & FAQs
								</a>

								<a href="#" class="flex-c-m p-lr-10 trans-04">
									My Account
								</a>

								<a href="#" class="flex-c-m p-lr-10 trans-04">
									EN
								</a>

								<a href="#" class="flex-c-m p-lr-10 trans-04">
									USD
								</a>
							</div>
						</li>
					</ul>

					<ul class="main-menu-m">
						<li>
							<a href="/index">Home</a>
							<span class="arrow-main-menu-m">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</span>
						</li>

						<li>
							<a href="/product">Shop</a>
						</li>
						<li>
							<a href="/blog">Review</a>
						</li>

						<li>
							<a href="/about">About</a>
						</li>

						<li>
							<a href="/contact">Contact</a>
						</li>
					</ul>
				</div>

				<!-- Modal Search -->
				<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
					<div class="container-search-header">
						<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
							<img src="images/icons/icon-close2.png" alt="CLOSE">
						</button>

						<form class="wrap-search-header flex-w p-l-15">
							<button class="flex-c-m trans-04">
								<i class="zmdi zmdi-search"></i>
							</button>
							<input class="plh3" type="text" name="search" placeholder="Search...">
						</form>
					</div>
				</div>
			</header>




			<!-- Title page -->
			<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url('images/bg-01.jpg');">
				<h2 class="ltext-105 cl0 txt-center">
					About
				</h2>
			</section>	


			<!-- Content page -->

			<div class="container mt-5 mb-5">
				<div class="row">
					<form action="/insert_data" method="POST" class="pt-5 pb-5">
						{{ csrf_field()}}
						<form>
							<div class="form-group">
								<label for="fname">First Name</label>
								<input type="text" class="form-control" id="fname" name ="fname"placeholder="Enter First Name">
							</div>
							<div class="form-group">
								<label for="lname">Last name</label>
								<input type="text" class="form-control" id="lname" name ="lname"placeholder="Enter Last Name">
							</div>
							<div class="form-group">
								<label for="phone">Phone</label>
								<input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone" placeholder="Enter Phone">
							</div>
							<div class="form-group">
								<label for="idcard">IdCard</label>
								<input type="text" class="form-control" id="idcard" name="idcard" aria-describedby="idcard" placeholder="Enter IdCard">
							</div>
							<div class="form-group">
								<label for="email">E-mail</label>
								<input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder="Enter E-mail">
							</div>
							<div class="form-group">
								<label for="address">Address</label>
								<input type="text" class="form-control" id="address" name="address" aria-describedby="address" placeholder="Enter Address">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name ="password"placeholder="Enter Password">
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</form>
				</div>
			</div>




			<!-- Back to top -->
			<div class="btn-back-to-top" id="myBtn">
				<span class="symbol-btn-back-to-top">
					<i class="zmdi zmdi-chevron-up"></i>
				</span>
			</div>

			<!--===============================================================================================-->	
			<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
			<!--===============================================================================================-->
			<script src="vendor/animsition/js/animsition.min.js"></script>
			<!--===============================================================================================-->
			<script src="vendor/bootstrap/js/popper.js"></script>
			<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
			<!--===============================================================================================-->
			<script src="vendor/select2/select2.min.js"></script>
			<script>
				$(".js-select2").each(function(){
					$(this).select2({
						minimumResultsForSearch: 20,
						dropdownParent: $(this).next('.dropDownSelect2')
					});
				})
			</script>
			<!--===============================================================================================-->
			<script src="vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
			<!--===============================================================================================-->
			<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
			<script>
				$('.js-pscroll').each(function(){
					$(this).css('position','relative');
					$(this).css('overflow','hidden');
					var ps = new PerfectScrollbar(this, {
						wheelSpeed: 1,
						scrollingThreshold: 1000,
						wheelPropagation: false,
					});

					$(window).on('resize', function(){
						ps.update();
					})
				});
			</script>
			<!--===============================================================================================-->
			<script src="js/main.js"></script>

		</body>
		</html>