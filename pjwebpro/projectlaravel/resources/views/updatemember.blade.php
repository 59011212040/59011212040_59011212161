<!DOCTYPE html>
<html lang="en">
<head>
  <title>About</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="{{asset('images/icons/favicon.png')}}"/>
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons-v1.0.0/icon-font.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
  <!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/animsition/css/animsition.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
  <!--===============================================================================================-->
</head>
<body class="animsition">

  <!-- Header -->
  <header class="header-v4">
    <!-- Header desktop -->
    <div class="container-menu-desktop">
      <!-- Topbar -->
      <div class="top-bar">
        <div class="content-topbar flex-sb-m h-full container">
          <div class="wrap-icon-header flex-w flex-r-m">

          </div>
        </div>
      </div>

      <div class="wrap-menu-desktop how-shadow1">
        <nav class="limiter-menu-desktop container">

          <!-- Logo desktop -->   
          <!-- Menu desktop -->
          <div class="menu-desktop">
            <ul class="main-menu">
              <li>
                <a href="/index">หน้าหลัก</a>
              </li>
              <li>
                <a href="/owner/self">ข้อมูลส่วนตัว</a>
              </li>
            </ul>
          </div>  

          <!-- Icon header -->
          <div class="wrap-icon-header flex-w flex-r-m">
           <li>
            <a href="/logout">Logout</a>
          </li>
        </div>
      </nav>
    </div>  
  </div>

  <!-- Header Mobile -->
  <div class="wrap-header-mobile">
    <!-- Logo moblie -->    
    <!-- Icon header -->
    <!-- Button show menu -->
    <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </div>
  </div>


  <!-- Menu Mobile -->
  <div class="menu-mobile">
    <ul class="topbar-mobile">
      <li>
        <div class="left-top-bar">
        </div>
      </li>
    </ul>

    <ul class="main-menu-m">
      <li>
        <a href="/index">หน้าหลัก</a>
      </li>
      <li>
        <a href="/owner/self">ข้อมูลส่วนตัว</a>
      </li>
    </ul>
  </div>



  <!-- Modal Search -->
  <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
    <div class="container-search-header">
      <button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
        <img src="{{asset('images/icons/icon-close2.png')}}" alt="CLOSE">
      </button>

      <form class="wrap-search-header flex-w p-l-15">
        <button class="flex-c-m trans-04">
          <i class="zmdi zmdi-search"></i>
        </button>
        <input class="plh3" type="text" name="search" placeholder="Search...">
      </form>
    </div>
  </div>
</header>

<!-- Cart -->
<!-- Title page -->
<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url({{ asset('images/bg-01.jpg') }});">
  <h2 class="ltext-105 cl0 txt-center">
    WELCOME <h5 class="ltext-105 cl0 txt-center text-success">{{ session('username')}}</h5>
  </h2>
</section>  

<br><br>
<div class="container">
  <div class="row main">
    <div class="col-lg-3">
    </div>
    <div class="main-login main-center col-lg-6">
      <h2 class="text-center">แก้ไขข้อมูลสมาชิก</h2>
      <form action="/update_data5" method="POST" class="pt-5 pb-5">
        <input type="hidden" name="id" value="<?=$members[0]->id?>">
        {{ csrf_field()}}
        <div class="form-group">
          <label for="username" class="cols-sm-2 control-label">Username</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="Username" id="Username"  placeholder="Enter your Username" value="<?=$members[0]->Username?>" >
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">First Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="fname" id="fname"  placeholder="Enter your First Name" value="<?=$members[0]->mFirstname?>" >
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Last Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="lname" id="lname"  placeholder="Enter your Last Name" value="<?=$members[0]->mLastname?>">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">Phone</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="phone" id="phone"  placeholder="Enter your Phone" value="<?=$members[0]->mPhone?>" >
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="cols-sm-2 control-label">ID Card</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="idcard" id="idcard"  placeholder="Enter your ID Card" value="<?=$members[0]->mIdcard?>">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">Your Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
              <input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" value="<?=$members[0]->mEmail?>" >
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="address" class="cols-sm-2 control-label">Address</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="address" id="address"  placeholder="Enter your Address" value="<?=$members[0]->mAddress?>">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="password" class="cols-sm-2 control-label">Password</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
              <input type="text" class="form-control" name="password" id="password"  placeholder="Enter your Password" value="<?=$members[0]->Password?>">
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>


<!-- Content page -->
<!-- Back to top -->
<div class="btn-back-to-top" id="myBtn">
  <span class="symbol-btn-back-to-top">
    <i class="zmdi zmdi-chevron-up"></i>
  </span>
</div>

<!--===============================================================================================--> 
<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script>
  $(".js-select2").each(function(){
    $(this).select2({
      minimumResultsForSearch: 20,
      dropdownParent: $(this).next('.dropDownSelect2')
    });
  })
</script>
<!--===============================================================================================-->
<script src="{{asset('vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script>
  $('.js-pscroll').each(function(){
    $(this).css('position','relative');
    $(this).css('overflow','hidden');
    var ps = new PerfectScrollbar(this, {
      wheelSpeed: 1,
      scrollingThreshold: 1000,
      wheelPropagation: false,
    });

    $(window).on('resize', function(){
      ps.update();
    })
  });
</script>
<!--===============================================================================================-->
<script src="{{asset('js/main.js')}}"></script>

</body>
</html>